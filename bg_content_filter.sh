#!/bin/sh
# To be called with
#  -f ${sender} -- ${recipient} ${user} ${domain}

FILTER=/usr/bin/bogofilter
FILTER_DIR=/var/spool/filter
# WARNING! The -i is crucial, else you may see
# messages truncated at the first period that is alone on a line
# (which can happen with several kinds of messages, particularly
# quoted-printable)
# -G is ignored before Postfix 2.3 and tells it that the message
# does not originate on the local system (Gateway submission),
# so Postfix avoids some of the local expansions that can leave
# misleading traces in headers, such as local address
# canonicalizations.
POSTFIX="/usr/sbin/sendmail -G -i"

while getopts "f:" flag; do
    case "$flag" in
        f) SENDER=$OPTARG
            ;;
    esac
done

logger -p mail.debug -t bg_content_filter "args: $@"

RECIPIENT=${@:$OPTIND:1}
USER=${@:$OPTIND+1:1}
DOMAIN=${@:$OPTIND+2:1}

logger -p mail.debug -t bg_content_filter \
	"args: RECIPIENT = $RECIPIENT, USER = $USER, DOMAIN = $DOMAIN"

export BOGOFILTER_DIR="/srv/vmail/$DOMAIN/users/$USER/bogofilter"
logger -p mail.debug -t bg_content_filter "BOGOFILTER_DIR = $BOGOFILTER_DIR"

# In case bogofilter directory is not present, user doesn't asks for
# using the filter.
if [ ! -d $BOGOFILTER_DIR ] ; then
    $POSTFIX -f ${SENDER} -- ${RECIPIENT}
    exit $?
fi

# Exit codes from <sysexits.h>
EX_TEMPFAIL=75  # temporary error, return message to the queue
EX_UNAVAILABLE=69  # mail rejected, bounce (DON'T USE!)

cd $FILTER_DIR || \
    { echo $FILTER_DIR does not exist; exit $EX_TEMPFAIL; }

# Clean up when done or when aborting.
TEMPFILE=$(mktemp -p $FILTER_DIR bgfilter_msg.XXXXXX )
trap "rm -f ${TEMPFILE} ; exit $EX_TEMPFAIL" 0 1 2 3 15

rm -f ${TEMPFILE} || exit $EX_TEMPFAIL

# bogofilter -e returns: 0 for OK, nonzero for error
# Run message through the bogofilter, don’t learn, just add
# the appropriate X-Bogosity header.
$FILTER -d ${BOGOFILTER_DIR} -p -e > ${TEMPFILE} || exit $EX_TEMPFAIL

BOGOSITY=$(grep 'X-Bogosity' ${TEMPFILE})

logger -p mail.debug -t bg_content_filter "X-Bogosity: $BOGOSITY"

exec <${TEMPFILE} || exit $EX_TEMPFAIL
rm -f ${TEMPFILE} # safe, we hold the file descriptor
exec $POSTFIX -f ${SENDER} -- ${RECIPIENT}
exit $EX_TEMPFAIL
